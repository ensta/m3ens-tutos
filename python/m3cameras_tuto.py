#!/usr/bin/python
'''
Created on Apr 2, 2013

@author: meka
'''
from m3perception import m3cameras
#from m3perception import m3kinect
import time

if __name__ == '__main__':
    cam = m3cameras.M3cameras()
    cam.wait_until_ready()

    while cam.is_alive():
        try:
             cam.show_both()
             time.sleep(0.25)
            #cam.left_th.wtf()
            #cam.right_th.wtf()
        except KeyboardInterrupt:
            print 'Catching ctrl+c'
    cam.stop()
    print 'Camera tuto stopped'
    exit()




