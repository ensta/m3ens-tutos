'''
Created on Apr 3, 2013

@author: meka
'''
#from m3perception import m3cameras
from m3perception import m3kinect
import time

if __name__ == '__main__':
    kin = m3kinect.M3kinect()
    kin.wait_until_ready()
    while kin.is_alive():
        try:
             kin.show_rgb()
             kin.show_depth()
             time.sleep(0.25)
        except KeyboardInterrupt:
            pass
    kin.stop()
    print 'Kinect tuto stopped'
    exit()