'''
Created on May 7, 2013

@author: meka
'''

import m3.rt_proxy as m3p
import m3.humanoid as m3h
import m3.toolbox as m3t


import numpy as np
import time

class M3Proc:
    def __init__(self):
        self.proxy = m3p.M3RtProxy()
        self.proxy.start()
        bot_name = m3t.get_robot_name()
        self.bot = m3h.M3Humanoid(bot_name)
        self.proxy.subscribe_status(self.bot)
        self.proxy.publish_param(self.bot)
        self.proxy.publish_command(self.bot)
        self.bot.set_motor_power_on()
        self.proxy.make_operational_all()
        self.shutdown_shared_memory()
        self.proxy.step()
        
    def shutdown_shared_memory(self):
        humanoid_shm_names=self.proxy.get_available_components('m3humanoid_shm')
        if len(humanoid_shm_names) > 0:
            self.proxy.make_safe_operational(humanoid_shm_names[0])
            
    def start(self):
        self.bot.set_mode_theta_gc('right_arm')
        self.bot.set_slew_rate_proportion('right_arm', [0.5]*7)
        self.bot.set_stiffness('right_arm', [0.6]*7)
        self.bot.set_theta_deg('right_arm', [0.0,25.0,10.0,35.0,0.0,1.0,00.0])
        self.proxy.step()
        raw_input("Press Enter to stop the demo")
        
    def stop(self):
        self.proxy.stop()

if __name__ == '__main__':
    t = M3Proc()
    try:
        t.start()
    except rospy.ROSInterruptException:
        pass
    t.stop()
    print 'Exit'     