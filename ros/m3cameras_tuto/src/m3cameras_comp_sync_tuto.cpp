#include <ros/ros.h>
#include <std_msgs/String.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
/// This tutorial shows how to get the m3 cameras iamges through a ROS node
/// That node subscribes to both cameras topics and convert the "ROS-image" message into an OpenCV cv::Mat

void imagesCallback(const sensor_msgs::ImageConstPtr& left_msg, const sensor_msgs::ImageConstPtr& right_msg)
{
  cv_bridge::CvImagePtr cv_ptr_left,cv_ptr_right;
  try
  {
      cv_ptr_left = cv_bridge::toCvCopy(left_msg, sensor_msgs::image_encodings::BGR8);
      cv_ptr_right = cv_bridge::toCvCopy(right_msg, sensor_msgs::image_encodings::BGR8);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  cv::Mat leftImage = cv_ptr_left->image;
  cv::Mat rightImage = cv_ptr_right->image;
  cv::imshow("Right camera",rightImage);
  cv::imshow("Left camera",leftImage);
  cv::waitKey(3);
}
int main(int argc, char **argv)
{
  ros::init(argc, argv, "m3cameras_tuto_cpp");
  ros::NodeHandle nh;
  /// One way of doing it is two subscribers for each image, but usually we want the two at the same time
  /// So we're gonna use the ROS synchronizer
  /// So we have two subscribers -> one callback !
  /// The counterpart is that it can drop a few images to sync it.
  message_filters::Subscriber<sensor_msgs::Image> left_sub(nh, "/meka_left_comp", 1);
  message_filters::Subscriber<sensor_msgs::Image> right_sub(nh, "/meka_right_comp", 1);
  message_filters::TimeSynchronizer<sensor_msgs::Image, sensor_msgs::Image> sync(left_sub, right_sub, 10);
  sync.registerCallback(boost::bind(&imagesCallback, _1, _2));
  ros::spin();
  cv::destroyAllWindows();
  return 0;
}
