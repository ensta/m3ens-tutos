#include <ros/ros.h>
#include <std_msgs/String.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
/// This tutorial shows how to get the m3 cameras iamges through a ROS node
/// That node subscribes to both cameras topics and convert the "ROS-image" message into an OpenCV cv::Mat
using namespace cv;
int thresh = 100;
int max_thresh = 255;
RNG rng(12345);

void contours(cv::Mat& image_src,cv::Mat& src_gray)
{
    Mat canny_output;
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    /// Detect edges using canny
    Canny( src_gray, canny_output, thresh, thresh*2, 3 );
    /// Find contours
    findContours( canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
    /// Draw contours
    Mat drawing = image_src.clone();//Mat::zeros( canny_output.size(), CV_8UC3 );
    for( unsigned int i = 0; i< contours.size(); i++ )
       {
         Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
         drawContours( drawing, contours, i, color, 2, 8, hierarchy, 0, Point() );
       }

    /// Show in a window
    imshow( "Left+contours", drawing );
}

void leftCamCallback(const sensor_msgs::ImageConstPtr& msg)
{
  cv_bridge::CvImagePtr cv_ptr;
  try
  {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge (left) exception: %s", e.what());
    return;
  }
  /// If sucess, then get the cv::Mat inside
  cv::Mat leftImage = cv_ptr->image;
  /// Convert image to gray and blur it
  cv::Mat src_gray;
  cvtColor( leftImage, src_gray, CV_BGR2GRAY );
  blur( src_gray, src_gray, Size(3,3) );
  contours(leftImage,src_gray);

}

void rightCamCallback(const sensor_msgs::ImageConstPtr& msg)
{
  cv_bridge::CvImagePtr cv_ptr;
  try
  {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge (right) exception: %s", e.what());
    return;
  }
  /// If sucess, then get the cv::Mat inside
  cv::Mat rightImage = cv_ptr->image;
  /// Do_some_stuff_to_it(rightImage)
  /// show it !
  cv::imshow("Right camera",rightImage);
  cv::waitKey(3);
}

void thresh_callback(int, void* )
{
    ROS_INFO("Changing the threshold !");
}

int main(int argc, char **argv)
{
  cv::namedWindow("Canny");
  createTrackbar( " Canny thresh:", "Canny", &thresh, max_thresh, thresh_callback );

  ros::init(argc, argv, "m3cameras_tuto_cpp");
  ros::NodeHandle nh;
  /// One way of doing it is two subscribers for each image, but usually we want the two at the same time
  /// So we're gonna use the ROS synchronizer
  ros::Subscriber leftCamSub = nh.subscribe("/meka_left_comp", 1, leftCamCallback);
  ros::Subscriber rightCamSub = nh.subscribe("/meka_right_comp", 1, rightCamCallback);
  ros::spin();
  cv::destroyAllWindows();
  return 0;
}
