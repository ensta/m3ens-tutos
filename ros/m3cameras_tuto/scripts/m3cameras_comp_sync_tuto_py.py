#!/usr/bin/env python
'''
Created on Mar 19, 2013

@author: meka
'''
import roslib; roslib.load_manifest('m3cameras_tuto')
import rospy
from std_msgs.msg import String
import message_filters
from sensor_msgs.msg import Image
import cv as cv
import cv2 as cv2
from cv_bridge import CvBridge, CvBridgeError
import numpy as np

threshold_bin = 127

def convert_to_gray(image):
    return cv.fromarray(cv2.cvtColor(np.asarray(image[:,:]), cv2.COLOR_BGR2GRAY))

def convert_to_gray_(img):
    gray = cv.CreateImage((img.width,img.height), 8, 1)
    cv.CvtColor(img, gray, cv.CV_BGR2GRAY)
    return gray  

def threshold(imgray,th,max,dst):
    assert(imgray.channels == 1)
    imgray_npy = np.asanyarray(imgray[:,:])
    ret,thresh = cv2.threshold(imgray_npy,th,max,dst)
    return cv.fromarray(thresh)

def threshold_(imgray,th,max,dst):
    assert(imgray.channels == 1)
    thresh = cv.CreateImage((imgray.width,imgray.height), 8, 1)
    cv.Threshold(imgray,thresh,th,max,dst)
    return thresh

def findContours(thresh):
    contours, hierarchy = cv2.findContours(np.asarray(thresh[:,:]),cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    return contours, hierarchy

def processing(images):
    nb = 0
    for img in images:
        imgray = convert_to_gray_(img)
        thresh = threshold_(imgray,threshold_bin,255,0)
        contours, hierarchy = findContours(thresh)
        cv2.drawContours(np.asarray(img[:,:]),contours,-1,(0,0,255),2)
        cv.ShowImage("image"+str(nb),img)
        nb=nb+1
        cv.WaitKey(3)
 
def on_trackbar (position):
    global threshold_bin
    threshold_bin=position

def callback(left_image_msg,right_image_msg):
    try:
        bridge = CvBridge()
        left_image=bridge.imgmsg_to_cv(left_image_msg, "bgr8")
        right_image= bridge.imgmsg_to_cv(right_image_msg, "bgr8")
    except CvBridgeError, e:
      print e
    cv.NamedWindow("image0")
    cv.CreateTrackbar("threshold", "image0", threshold_bin, 255, on_trackbar)
    processing([left_image,right_image])


if __name__ == '__main__':
    rospy.init_node('m3cameras_comp_tuto_py', anonymous=True)
    leftImageSub = message_filters.Subscriber('/meka_left_comp', Image)
    rightImageSub = message_filters.Subscriber('/meka_right_comp', Image)
    ts = message_filters.TimeSynchronizer([leftImageSub, rightImageSub], 10)
    ts.registerCallback(callback)
    rospy.spin()
