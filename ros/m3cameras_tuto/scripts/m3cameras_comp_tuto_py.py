#!/usr/bin/env python
'''
Created on Mar 19, 2013

@author: meka
'''

import roslib; roslib.load_manifest('m3cameras_tuto')
import rospy
from std_msgs.msg import String
import message_filters
from sensor_msgs.msg import Image
import cv
import cv2
from cv_bridge import CvBridge, CvBridgeError
import numpy as npy

harris_value=3

def left_callback(left_image_msg):
    try:
        bridge = CvBridge()
        left_image = bridge.imgmsg_to_cv(left_image_msg, "bgr8")
    except CvBridgeError, e:
      print e
    #Do stuff with the image
    cv.ShowImage("Left camera", left_image)
    cv.WaitKey(3)

def convert_to_gray(image):
    newFrameImage32F = cv.CreateImage((image.width, image.height), cv.IPL_DEPTH_32F, 3)
    cv.ConvertScale(image,newFrameImage32F)
    
    newFrameImageGS_32F = cv.CreateImage ((image.width, image.height), cv.IPL_DEPTH_32F, 1)
    cv.CvtColor(newFrameImage32F,newFrameImageGS_32F,cv.CV_RGB2GRAY)
    
    image_gray = cv.CreateImage ((image.width, image.height), cv.IPL_DEPTH_8U, 1)
    cv.ConvertScale(newFrameImageGS_32F,image_gray)
    return image_gray
    
def right_callback(right_image_msg):
    try:
        bridge = CvBridge()
        right_image = bridge.imgmsg_to_cv(right_image_msg, "bgr8")
    except CvBridgeError, e:
      print e

    right_image_gs = convert_to_gray(right_image)
    
    cornerMap = cv.CreateMat(right_image.height, right_image.width, cv.CV_32FC1)
    cv.CornerHarris(right_image_gs,cornerMap,harris_value)

    for y in range(0, right_image.height):
        for x in range(0, right_image.width):
            harris = cv.Get2D(cornerMap, y, x) 
            if harris[0] > 10e-06:
                cv.Circle(right_image,(x,y),2,cv.RGB(155, 0, 25))
    cv.ShowImage('Right image + Harris', right_image)
    cv.WaitKey(3)

def trackbarCallback(position):
    global harris_value
    harris_value = position
if __name__ == '__main__':    
    cv.NamedWindow('Right image + Harris', cv.CV_WINDOW_AUTOSIZE)
    cv.CreateTrackbar("Harris block value", 'Right image + Harris', harris_value, 20, trackbarCallback)
    
    rospy.init_node('m3cameras_comp_tuto_py', anonymous=True)
    rospy.Subscriber('/meka_left_comp', Image,left_callback,queue_size=1, buff_size=2**24)
    rospy.Subscriber('/meka_right_comp', Image,right_callback,queue_size=1, buff_size=2**24)
    rospy.spin()
