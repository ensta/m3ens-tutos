#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <iostream>
#include <string>
#include <vector>

using namespace std;


std::vector<string> getListOfFrames(std::string allFrames,std::string topicName)
{
    std::vector<string> listOfFrames;
    unsigned found = 0;
    do
    {
        found = allFrames.find(topicName,found+1);
        if(found!=std::string::npos)
        {
            unsigned next_space_pos = allFrames.find(" ",found+topicName.length());
            listOfFrames.push_back(allFrames.substr(found,next_space_pos-found));
        }
    }
    while(found!=std::string::npos);
    return listOfFrames;
}


int main(int argc, char** argv){
  ros::init(argc, argv, "tf_listener");

  ros::NodeHandle node;
  tf::TransformListener listener;

  ros::Rate rate(10.0);
  std::string allFrames;
  while (node.ok()){
    tf::StampedTransform transform;
    try{
        allFrames = listener.allFramesAsString();
        std::string right_hand("/right_hand");
        std::string left_hand("/left_hand");
        vector<string> right_hands = getListOfFrames(allFrames,right_hand);
        vector<string> left_hands = getListOfFrames(allFrames,left_hand);
        std::vector<string> allHands=right_hands;
        allHands.insert(allHands.end(), left_hands.begin(), left_hands.end());
        cout<<"All Hands : "<<endl;
        for (unsigned int i=0;i<allHands.size();++i)
            cout<<"* "<<allHands[i]<<endl;
    }
    catch (tf::TransformException ex){
        ROS_ERROR("ERROR : %s",ex.what());
    }
    rate.sleep();
  }
  return 0;
};
