#include "ros/ros.h"
#include <sensor_msgs/PointCloud2.h>
#include <pcl/ros/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include "sensor_msgs/Image.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/opencv.hpp>


/// Tuto to get the kinect point cloud (and create an RGB 2D image with it)


namespace enc = sensor_msgs::image_encodings;
using namespace cv;
using namespace std;

void getCloudCallback(const sensor_msgs::PointCloud2& cloud_msg)
{
    pcl::PointCloud<pcl::PointXYZRGB> cloud;
    pcl::fromROSMsg(cloud_msg,cloud);
    cv::Mat imageBGR(cloud.height,cloud.width,CV_8UC3);
    for(unsigned int i=0;i<cloud.height;i++)
    {
        for(unsigned int j=0;j<cloud.width;j++)
        {
            pcl::PointXYZRGB p = cloud.at(j,i);
            Vec3b bgr(p.b,p.g,p.r);
            imageBGR.at<Vec3b>(i,j) = bgr;
        }
    }
    imshow("Kinect RGB from point cloud",imageBGR);
    cv::waitKey(3);
}


int main( int argc, char** argv )
{
    ros::init(argc, argv, "m3kinect_tuto");
    ros::NodeHandle n;
    ros::Subscriber cloudSub = n.subscribe("/camera/depth_registered/points", 1, getCloudCallback);
    ros::spin();
    return 0;
}
